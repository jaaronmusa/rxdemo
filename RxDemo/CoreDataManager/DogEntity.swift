//
//  DogEntity.swift
//  RxDemo
//
//  Created by Aaron Musa on 04/08/2018.
//  Copyright © 2018 Aaron Musa. All rights reserved.
//

import UIKit
import CoreData

class DogEntity: NSManagedObject {
    
    let container = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
    let viewContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //MARK: Add Dog
    func add(dog: Dog,
            success: @escaping () ->Void) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DogEntity")
        request.predicate = NSPredicate(format: "id = %@", dog.id ?? "")
        request.returnsObjectsAsFaults = false
        
        do {
            var dogEntity: DogEntity!
            let result = try viewContext.fetch(request)
            let willInsert = result.count == 1 ? false : true
            if willInsert {
                dogEntity = DogEntity(context: viewContext)
            }
            else{
                if let results = result as? [NSManagedObject], let dogResult = results.first as? DogEntity {
                    dogEntity = dogResult
                }
            }
            
            dogEntity.id = dog.id
            dogEntity.name = dog.name
            dogEntity.breed = dog.breed
            dogEntity.energy = dog.energy
            dogEntity.imageData = dog.imageData
            
            
        } catch {
            print("Failed")
        }
        
        
        
        do {
            try viewContext.save()
            success()
            
        } catch {
            print("failed")
        }
    }
    
    func fetchAll(success: @escaping ([NSManagedObject]) -> Void) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DogEntity")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try viewContext.fetch(request)
            if let dogs = result as? [NSManagedObject] {
                success(dogs)
            }
            
        } catch {
            print("Failed")
        }
    }
    
    func fetch(with id: String, success: @escaping (NSManagedObject) -> Void) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DogEntity")
        request.predicate = NSPredicate(format: "id = %@", id)
        request.returnsObjectsAsFaults = false
        do {
            let result = try viewContext.fetch(request)
            if let dogs = result as? [NSManagedObject] {
                guard let dog = dogs.first else { return }
                success(dog)
            }
        } catch {
            print("Failed")
        }
    }
    
    func delete(with id: String, success: @escaping () -> Void) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DogEntity")
        request.predicate = NSPredicate(format: "id = %@", id)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try viewContext.fetch(request)
            if let dogs = result as? [NSManagedObject] {
                guard let dog = dogs.first else { return }
                viewContext.delete(dog)
                try viewContext.save()
                success()
            }
        } catch {
            print("Failed")
        }
    }
    
    func deleteAll(success: @escaping () -> Void) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DogEntity")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try viewContext.fetch(request)
            if let dogs = result as? [NSManagedObject] {
                for dog in dogs {
                    viewContext.delete(dog)
                    try viewContext.save()
                    success()
                }
            }
        } catch {
            print("Failed")
        }
    }
}
