//
//  CoreDataManager.swift
//  RxDemo
//
//  Created by Aaron Musa on 02/08/2018.
//  Copyright © 2018 Aaron Musa. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol DataRepositoryProtocol {
    func addDog(dog: Dog, success: @escaping () -> Void)
    func fetchDogs(success: @escaping ([Dog]) -> Void)
    func deleteDog(with id: String, success: @escaping () -> Void)
    func deleteDogs(success: @escaping () -> Void)
}

class DataRepository: DataRepositoryProtocol {
    
    static let shared = DataRepository()
    let dogEntity = DogEntity()

    //Dog Entity
    func addDog(dog: Dog, success: @escaping () -> Void){
        dogEntity.add(dog: dog) {
            success()
        }
    }

    
    func fetchDogs(success: @escaping ([Dog]) -> Void) {
        var dogs = [Dog]()
        dogEntity.fetchAll { (dogsData) in
            for dogData in dogsData {
                if let dogEntity = dogData as? DogEntity {
                    let dog = Dog(dogEntity: dogEntity)
                    dogs.append(dog)
                }
            }
            success(dogs)
        }
    }
    
    func deleteDog(with id: String, success: @escaping () -> Void) {
        dogEntity.delete(with: id) {
            success()
        }
    }
    
    func deleteDogs(success: @escaping () -> Void) {
        dogEntity.deleteAll {
            success()
        }
    }
}
