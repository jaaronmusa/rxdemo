//
//  Dog.swift
//  RxDemo
//
//  Created by Aaron Musa on 02/08/2018.
//  Copyright © 2018 Aaron Musa. All rights reserved.
//

import Foundation

class Dog {
    
    var id: String?
    var name: String?
    var image: String?
    var breed: String?
    var energy: String?
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
    var imageData: Data?
    
    init(json: [String: Any]) {
        id = json["id"] as? String
        name = json["name"] as? String
        image = json["image"] as? String
        breed = json["breed"] as? String
        energy = json["energy"] as? String
        createdAt = json["createdAt"] as? Date
        updatedAt = json["updatedAt"] as? Date
        deletedAt = json["deletedAt"] as? Date
        
        imageData = json["imageData"] as? Data
    }
    
    init(dogEntity: DogEntity) {
        id = dogEntity.id
        name = dogEntity.name
        image = dogEntity.image
        breed = dogEntity.breed
        energy = dogEntity.energy
        createdAt = dogEntity.createdAt
        updatedAt = dogEntity.updatedAt
        deletedAt = dogEntity.deletedAt
        
        imageData = dogEntity.imageData
    }
}
