//
//  AddDogViewModel.swift
//  RxDemo
//
//  Created by Aaron Musa on 05/08/2018.
//  Copyright © 2018 Aaron Musa. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AddDogViewModel {
    
    var repository: RepositoryProtocol!
    let dataRepository: DataRepositoryProtocol!
    
    let bag = DisposeBag()
    
    var existingDog = BehaviorRelay<Dog?>(value: nil)
    
    var id = BehaviorRelay<String>(value: UUID().uuidString)
    var name = BehaviorRelay<String>(value: "")
    var energy = BehaviorRelay<String>(value: "")
    var breed = BehaviorRelay<String>(value: "")
    var displayPhotoUrl = BehaviorRelay<String>(value: "")
    var displayPhotoData = BehaviorRelay<Data>(value: Data())
    
    init(repository: RepositoryProtocol, dataRepository: DataRepositoryProtocol) {
        self.repository = repository
        self.dataRepository = dataRepository
    }
    
    func addDog() -> Observable<Any> {
        return Observable.create({ observer in
            let data: [String : Any] = ["id": self.id.value,
                                        "name": self.name.value,
                                        "image": self.displayPhotoUrl.value,
                                        "breed": self.breed.value,
                                        "energy": self.energy.value,
                                        "imageData": self.displayPhotoData.value]
            let dog = Dog(json: data)
            
            self.dataRepository.addDog(dog: dog, success: {
                observer.onNext(())
                observer.onCompleted()
            })
            
            return Disposables.create()
        })
    }
    
    func fetchDogs() -> Observable<[Dog]> {
        return Observable.create({ observer in
            self.dataRepository.fetchDogs(success: { (dogs) in
                observer.onNext(dogs)
            })
            
            return Disposables.create()
        })
    }
}










