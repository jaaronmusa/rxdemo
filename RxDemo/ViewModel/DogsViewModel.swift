//
//  TableViewSampleViewController.swift
//  RxDemo
//
//  Created by Aaron Musa on 21/07/2018.
//  Copyright © 2018 Aaron Musa. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class DogsViewModel {
    
    var repository: RepositoryProtocol!
    var dataRepository: DataRepositoryProtocol!
    
    var dogs = BehaviorRelay<[Dog]>(value: [])
    
    private let bag = DisposeBag()
    
    
    init(repository: RepositoryProtocol, dataRepository: DataRepositoryProtocol) {
        self.repository = repository
        self.dataRepository = dataRepository
    }
    
    func fetchDogs(){
        dataRepository.fetchDogs { (dogs) in
            self.dogs.accept(dogs)
        }
    }
    
    func deleteDog(with id: String?) -> Observable<Any>{
        return Observable.create({ observer in
            self.dataRepository.deleteDog(with: id ?? "") {
                observer.onNext(())
                observer.onCompleted()
            }

            return Disposables.create()
        })
        
    }
    
//    func fetchDogs() -> Observable<Any> {
//        return Observable.create({ observer in
//
//            return Disposables.create()
//        })
//    }
    
}
