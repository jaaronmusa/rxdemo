//
//  ViewControllerHelper.swift
//  RxDemo
//
//  Created by Aaron Musa on 04/08/2018.
//  Copyright © 2018 Aaron Musa. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func addTapRecognizerForDismissingKeyboard(inView view: UIView) {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        gestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
