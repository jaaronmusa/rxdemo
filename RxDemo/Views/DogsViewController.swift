//
//  TableSampleViewController.swift
//  RxDemo
//
//  Created by Aaron Musa on 21/07/2018.
//  Copyright © 2018 Aaron Musa. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DogsViewController: UIViewController {

    @IBOutlet weak var dogsTableView: UITableView!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    let bag = DisposeBag()
    
    var actionSheet: UIAlertController?
    
    var viewModel: DogsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = DogsViewModel(repository: Repository.shared, dataRepository: DataRepository.shared)
        
        addButton.rx.tap.subscribe({ _ in
            if let addDogViewController = StoryBoard.main.instantiateViewController(withIdentifier: "AddDogViewController") as? AddDogViewController {
                self.show(addDogViewController, sender: self)
            }
        }).disposed(by: bag)
        
        viewModel.dogs.bind(to: dogsTableView.rx.items(cellIdentifier: "DogCell", cellType: DogCell.self)) { row, dog, cell in
            cell.nameLabel.text = dog.name
            cell.breedLabel.text = dog.breed
            cell.energyLabel.text = dog.energy
            cell.displayPhotoImageView.image = UIImage(data: dog.imageData ?? Data())
            
        }.disposed(by: bag)
        
        dogsTableView.rx.itemSelected.subscribe(onNext: { indexPath in
            self.setupActionSheet()
            self.actionSheet?.view.tag = indexPath.row
            self.dogsTableView.deselectRow(at: indexPath, animated: true)
        }).disposed(by: bag)
        
        backButton.rx.tap.subscribe(onNext: { _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        viewModel.fetchDogs()
    }
}

extension DogsViewController {
    //Custom Actions
    
    func setupActionSheet() {
        actionSheet = UIAlertController(title: "Action", message: "Want changes?", preferredStyle: .actionSheet)
        let delete = UIAlertAction(title: "Delete", style: .destructive) { _ in
            let index = self.actionSheet?.view.tag ?? 0
            
            var dogs = self.viewModel.dogs.value
            self.viewModel.deleteDog(with: dogs[index].id).subscribe(onNext: { _ in
                dogs.remove(at: index)
                self.viewModel.dogs.accept(dogs)
            }).disposed(by: self.bag)
        }
        
        let edit = UIAlertAction(title: "Edit", style: .default) { _ in
            let index = self.actionSheet?.view.tag ?? 0
            
            let dogs = self.viewModel.dogs.value
            if let addDogViewController = StoryBoard.main.instantiateViewController(withIdentifier: "AddDogViewController") as? AddDogViewController {
                addDogViewController.viewModel = AddDogViewModel(repository: Repository.shared, dataRepository: DataRepository.shared)
                addDogViewController.viewModel.existingDog.accept(dogs[index])
                self.show(addDogViewController, sender: self)
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            self.actionSheet?.dismiss(animated: true, completion: nil)
        }
        
        actionSheet?.addAction(edit)
        actionSheet?.addAction(delete)
        actionSheet?.addAction(cancel)
        
        present(actionSheet!, animated: true, completion: nil)
    }
}

