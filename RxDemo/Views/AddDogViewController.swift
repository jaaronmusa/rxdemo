//
//  AddDogViewController.swift
//  RxDemo
//
//  Created by Aaron Musa on 01/08/2018.
//  Copyright © 2018 Aaron Musa. All rights reserved.
//

import UIKit
import RxSwift

class AddDogViewController: UIViewController {

    @IBOutlet weak var displayPhotoImageView: UIImageView!
    @IBOutlet weak var editImageButton: UIButton!
    @IBOutlet weak var energyTextField: UITextField!
    @IBOutlet weak var breedTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    var viewModel: AddDogViewModel!
    
    let bag = DisposeBag()
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapRecognizerForDismissingKeyboard(inView: view)
        
        if viewModel == nil {
            viewModel = AddDogViewModel(repository: Repository.shared, dataRepository: DataRepository.shared)
        }
        
        nameTextField.rx.text.map { $0 ?? "" }.bind(to: viewModel.name).disposed(by: bag)
        energyTextField.rx.text.map { $0 ?? "" }.bind(to: viewModel.energy).disposed(by: bag)
        breedTextField.rx.text.map { $0 ?? "" }.bind(to: viewModel.breed).disposed(by: bag)
        
        viewModel.existingDog.subscribe(onNext: { dog in
            
            self.viewModel.id.accept(dog?.id ?? "")
            self.viewModel.name.accept(dog?.name ?? "")
            self.viewModel.energy.accept(dog?.energy ?? "")
            self.viewModel.breed.accept(dog?.breed ?? "")
            self.viewModel.displayPhotoUrl.accept(dog?.image ?? "")
            self.viewModel.displayPhotoData.accept(dog?.imageData ?? Data())
            
            self.viewModel.name.bind(to: self.nameTextField.rx.text).disposed(by: self.bag)
            self.viewModel.energy.bind(to: self.energyTextField.rx.text).disposed(by: self.bag)
            self.viewModel.breed.bind(to: self.breedTextField.rx.text).disposed(by: self.bag)
            self.viewModel.displayPhotoData.bind(onNext: { (imageData) in
                self.displayPhotoImageView.image = UIImage(data: imageData)
            }).disposed(by: self.bag)
            
        }).disposed(by: bag)
        
        editImageButton.rx.tap.subscribe(onNext: { _ in
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }).disposed(by: bag)
        
        saveButton.rx.tap.subscribe(onNext: { _ in
            self.viewModel.addDog().subscribe(onNext: { _ in
                self.dismissKeyboard()
                self.navigationController?.popViewController(animated: true)
            }).disposed(by: self.bag)
        }).disposed(by: bag)
    }
}

extension AddDogViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info["UIImagePickerControllerEditedImage"] as? UIImage, let imageUrl = info["UIImagePickerControllerImageURL"] as? URL {
            self.displayPhotoImageView.image = image
            self.viewModel.displayPhotoUrl.accept(imageUrl.absoluteString)
            
            let imageData = UIImageJPEGRepresentation(image, 0.8)
            self.viewModel.displayPhotoData.accept(imageData ?? Data())
        }
        picker.dismiss(animated: true, completion: nil)
    }
}











