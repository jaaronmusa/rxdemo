//
//  DogCell.swift
//  RxDemo
//
//  Created by Aaron Musa on 05/08/2018.
//  Copyright © 2018 Aaron Musa. All rights reserved.
//

import UIKit

class DogCell: UITableViewCell {
    
    @IBOutlet weak var displayPhotoImageView: UIImageView!
    
    @IBOutlet weak var energyLabel: UILabel!
    @IBOutlet weak var breedLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
